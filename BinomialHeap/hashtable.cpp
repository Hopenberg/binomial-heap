#include "HashTable.h"

HashTable::HashTable(int size) //1021
{
	this->size = size;
	tab = (BnHeap **)malloc(size * sizeof(BnHeap *));
	for (int i = 0; i < size; i++)
		tab[i] = NULL;
}
int HashTable::hashOne(int key)
{
	int value = key % size;
	return value;
}
int HashTable::hashTwo(int key)
{
	int value = 1 + (key % (size - 2));
	return value;
}
int HashTable::hash(int key, int iter)
{
	int one = hashOne(key);
	int two = hashTwo(key);
	int value = (one + (iter * two)) % size;
	return value;
}
int HashTable::insert(int key)
{
	int i = 0;
	while (i != size)
	{
		int j = hash(key, i);
		if (tab[j] == NULL) return j;
		i++;
	}
	return -1;
}
int HashTable::search(int key)
{
	int j;
	int i = 0;
	do
	{
		j = hash(key, i);
		if (tab[j] != NULL && tab[j]->number == key) return j;
		i++;
	} while (tab[j] != NULL || i != size);
	return -1;
}
HashTable::~HashTable()
{
	for (int i = 0; i < size; i++)
	{
		if (tab[i] != NULL)
			free(tab[i]);
	}
	free(tab);
}