#define _CRT_SECURE_NO_WARNINGS
// BinominalHeapOO.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include "BnHeap.h"
#include "HashTable.h"
using std::cout;
using std::endl;
using std::cin;

//#define TEST

int main()
{
#ifndef TEST
	int count;
	char cmd;
	int oldVal, newVal, group1, group2;
	int index, result, index2;
	BnHeap::bnNode *resultNode;
	HashTable *hash = new HashTable(2113);
	BnHeap::bnNode *test;
	BnHeap::bnNode *test2;
	cin >> count;
	for (int i = 0; i < count; i++)
	{
		cin >> cmd;
		switch (cmd)
		{
		case 'a': //insert
			cin >> group1 >> newVal;
			index = hash->search(group1);
			if (index == -1)
			{
				index = hash->insert(group1);
				hash->tab[index] = new BnHeap(group1);
			}
			hash->tab[index]->bnHeapInsert(newVal);
			break;
		case 'e': //extract
			cin >> group1;
			index = hash->search(group1);
			if (index == -1)
				cout << "na\n";
			else
			{
				resultNode = hash->tab[index]->extractMax();
				if (resultNode == NULL)
					cout << "na\n";
				else
					cout << resultNode->key << endl;
				free(resultNode);
			}
			break;
		case 'p': //print
			cin >> group1;
			break;
		case 'm': //merge
			cin >> group1 >> group2;
			if (group1 == group2)
				break;
			index = hash->search(group1);
			index2 = hash->search(group2);
			if (index2 == -1)
				;// cout << "na\n";
			else if (index == -1)
				;// cout << "na\n";
			else
			{
				hash->tab[index]->head = hash->tab[index]->bnHeapUnion(hash->tab[index]->head, hash->tab[index2]->head);
			}
			if (index2 != -1)
				hash->tab[index2]->head = NULL;
			//test = hash->tab[index]->head;
			//test2 = hash->tab[index2]->head;
			break;
		case 'i': //increase
				//cin >> group1 >> oldVal >> newVal;
			scanf("%i", &group1);
			scanf("%i", &oldVal);
			scanf("%i", &newVal);
			index = hash->search(group1);
			if (index == -1)
			{
				cout << "na\n";
			}
			else
				if (hash->tab[index]->increaseValue(oldVal, newVal) == -1)
					cout << "na\n";
			break;
		}
	}

#endif
	//#define TEST
#ifdef TEST

	BnHeap *heap = new BnHeap(2131);
	heap->bnHeapInsert(20);
	heap->bnHeapInsert(21);
	heap->bnHeapInsert(22);
	heap->bnHeapInsert(23);
	heap->bnHeapInsert(24);
	heap->bnHeapInsert(25);
	heap->bnHeapInsert(26);
	heap->bnHeapInsert(27);
	heap->bnHeapInsert(28);
	heap->bnHeapInsert(29);
	heap->bnHeapInsert(30);
	heap->bnHeapInsert(31);
	heap->bnHeapInsert(32);
	heap->bnHeapInsert(33);
	heap->bnHeapInsert(34);
	heap->bnHeapInsert(35);

	heap->increaseValue(2, 6969);

	cout << heap->extractMax() << endl;
#endif
	return 0;
}
