#pragma once
#include "BnHeap.h"

class HashTable
{
public:
	BnHeap **tab;
	int size;

	HashTable(int size);
	int insert(int key);
	int search(int key);
	int hashOne(int key);
	int hashTwo(int key);
	int hash(int key, int iter);
	~HashTable();
};