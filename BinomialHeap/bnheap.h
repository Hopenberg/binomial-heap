#pragma once
#include <iostream>
class BnHeap
{
public:
	struct bnNode
	{
		bnNode()
		{
			parent = NULL;
			child = NULL;
			sibling = NULL;
			degree = 0;
		}
		int key;
		int age;
		int degree;
		bnNode *parent;
		bnNode *child;
		bnNode *sibling;
	};
	bnNode *head;
	int age;
	int number;

	BnHeap(int groupNr);
	void treeLink(bnNode *y, bnNode *z);
	bnNode *bnHeapMax();
	bnNode *bnHeapUnion(bnNode *heap1, bnNode *heap2);
	bnNode *bnHeapMerge(bnNode *heap1, bnNode *heap2);
	void bnHeapInsert(int key);
	bnNode *extractMax();
	int increaseValue(int old, int newVal);
	bnNode *findNode(bnNode *head, int key);
};