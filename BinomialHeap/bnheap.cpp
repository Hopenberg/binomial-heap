#include "BnHeap.h"
#include <iostream>

BnHeap::BnHeap(int groupNr)
{
	this->number = groupNr;
	head = NULL;
	age = 0;
}
void BnHeap::treeLink(bnNode *newSon, bnNode *newParent)
{
	newSon->parent = newParent;
	newSon->sibling = newParent->child;
	newParent->child = newSon;
	newParent->degree += 1;
}
BnHeap::bnNode *BnHeap::bnHeapMerge(bnNode *heap1, bnNode *heap2)
{
	bnNode *head = NULL;
	bnNode *tail = NULL;
	bnNode *next = NULL;
	while (heap1 != NULL && heap2 != NULL)
	{
		if (heap1->degree <= heap2->degree)
		{
			next = heap1;
			heap1 = heap1->sibling;
		}
		else
		{
			next = heap2;
			heap2 = heap2->sibling;
		}
		if (head == NULL)
		{
			head = next;
			tail = next;
		}
		else
		{
			tail->sibling = next;
			tail = tail->sibling;
		}
	}
	if (heap1 == NULL)
	{
		bnNode *temp = heap1;
		heap1 = heap2;
		heap2 = temp;
	}
	if (head == NULL)
	{
		return heap1;
	}
	else
	{
		tail->sibling = heap1;
	}
	return head;
}
BnHeap::bnNode *BnHeap::bnHeapUnion(bnNode *heap1, bnNode *heap2)
{
	bnNode *head = bnHeapMerge(heap1, heap2);
	if (head == NULL) return NULL;
	bnNode *prevX = NULL;
	bnNode *x = head;
	bnNode *nextX = head->sibling;
	while (nextX != NULL)
	{
		if (x->degree != nextX->degree || (nextX->sibling != NULL && nextX->sibling->degree == x->degree))
		{
			prevX = x;
			x = nextX;
			nextX = nextX->sibling;
		}
		else if (x->key >= nextX->key)
		{
			if (x->key == nextX->key)
			{
				if (x->age < nextX->age)
				{
					x->sibling = nextX->sibling;
					nextX->sibling = NULL;
					treeLink(nextX, x);
					nextX = x->sibling;
				}
				else
				{
					if (prevX == NULL) head = nextX;
					else prevX->sibling = nextX;
					x->sibling = NULL;
					treeLink(x, nextX);
					x = nextX;
					nextX = nextX->sibling;
				}
			}
			else
			{
				x->sibling = nextX->sibling;
				nextX->sibling = NULL;
				treeLink(nextX, x);
				nextX = x->sibling;
			}

		}
		else
		{
			if (prevX == NULL) head = nextX;
			else prevX->sibling = nextX;
			x->sibling = NULL;
			treeLink(x, nextX);
			x = nextX;
			nextX = nextX->sibling;
		}
	}
	return head;
}
void BnHeap::bnHeapInsert(int key)
{
	bnNode *node = new bnNode();
	this->age++;
	node->age = age;
	node->key = key;
	if (this->head == NULL)
		this->head = node;
	else
		this->head = bnHeapUnion(this->head, node);
}
BnHeap::bnNode *BnHeap::bnHeapMax()
{
	bnNode *searchedNode = NULL;
	bnNode *x = head;
	int max = head->key;
	while (x != NULL)
	{
		if (x->key > max)
		{
			max = x->key;
			searchedNode = x;
		}
		x = x->sibling;
	}
	return searchedNode;
}
BnHeap::bnNode *BnHeap::extractMax()
{
	bnNode *prevX = NULL;
	bnNode *x = head;
	if (x == NULL)
		return NULL;
	int max = head->key;
	while (x != NULL)
	{
		if (x->key > max)
		{
			max = x->key;
		}
		x = x->sibling;
	}
	x = head;
	while (x != NULL)
	{
		if (x->key == max)
			break;
		prevX = x;
		x = x->sibling;
	}
	//usuniecie korzenia z listy korzeni this->head
	if (prevX != NULL)
	{
		prevX->sibling = x->sibling;
		x->sibling = NULL;
	}
	else
	{
		head = x->sibling;
		x->sibling = NULL;
	}
	BnHeap *tempHeap = new BnHeap(0);
	bnNode *children = x->child;

	if (x->child == NULL)
	{
		return x;
	}


	bnNode *tempIter = children->sibling;
	tempHeap->head = children;
	children->sibling = NULL;
	children->parent = NULL;

	while (tempIter != NULL)
	{
		bnNode *tempIter2 = tempIter;
		tempIter = tempIter->sibling;
		tempIter2->sibling = tempHeap->head;
		tempIter2->parent = NULL;
		tempHeap->head = tempIter2;
	}
	head = bnHeapUnion(head, tempHeap->head);


	return x;
}
BnHeap::bnNode *BnHeap::findNode(bnNode*head, int key)
{
	bnNode *x = head;
	bnNode *y = NULL;
	bnNode *z = NULL;
	while (x != NULL)
	{
		if (x->key == key)
			return x;
		y = x->child;
		while (y != NULL)
		{
			if (y->key == key)
				return y;
			z = y->sibling;
			while (z != NULL)
			{
				if (z->key == key)
					return z;
				if (z->child != NULL)
				{
					bnNode *deeper = findNode(z->child, key);
					if (deeper != NULL)
						return deeper;
				}
				z = z->sibling;
			}
			y = y->child;
		}
		x = x->sibling;
	}
	return NULL;
}
int BnHeap::increaseValue(int old, int newValue)
{
	bnNode *searched = findNode(this->head, old);
	if (searched == NULL)
		return -1;
	if (old <= newValue)
	{
		searched->key = newValue;
		bnNode *y = searched;
		bnNode *z = searched->parent;
		while ((z || z != NULL) && y->key > z->key)
		{
			int tempKeyY = y->key;
			int tempAgeY = y->age;
			y->key = z->key;
			y->age = z->age;
			z->key = tempKeyY;
			z->age = tempAgeY;
			bnNode* temp = y;
			y = z;
			z = y->parent;
		}
	}
	else
	{
		searched->key = 2147483640;
		bnNode *y = searched;
		bnNode *z = searched->parent;
		while ((z || z != NULL) && y->key > z->key)
		{
			int tempKeyY = y->key;
			int tempAgeY = y->age;
			y->key = z->key;
			y->age = z->age;
			z->key = tempKeyY;
			z->age = tempAgeY;
			bnNode* temp = y;
			y = z;
			z = y->parent;
		}
		extractMax();
		bnHeapInsert(newValue);
	}

	return 1;
}